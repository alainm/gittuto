#include <string>
#include "tex.hpp"


std::string greet() {
  return "what's up doc?";
}
/// Another comment line
/// \brief The original Tex Avery greeting
std::string taxpayers() {
  return "Hello Happy Tax Payers!";
}

std::string coffeedrinker() {
  return "If you drink too much coffee, you will not sleep";
}
